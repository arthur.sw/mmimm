let parameters = {
    thickness: 3.0, 
    diameter: 80.0, 
    interiorHeight: 33.0, 
    interiorWallThickness: 4.0,
    spaceBetweenTopWalls: 1.0,
    bearingHeight: 10.0, 
    bearingInnerDiameter: 17.0, 
    bearingOuterDiameter: 35.0,
    bearingEdgeThickness: 3.0,
    rotaryEncoderDiameter: 6.0,
    rotaryEncoderDOffset: 4.5,
    rotaryEncoderHeight: 10.0,
    rotaryEncoderHolderWidth: 15.0,
    rotaryEncoderHolderHoleDiameter: 8.0,
    rotaryEncoderNotchSize: 1.0,
    spaceBetweenTopAndBottom: 1.0,
    wraperStepSize: 8.0,
    wraperStepHeight: 3.0,
    wraperNCircles: 4,
    wraperCirclesSize: 2.0,
    wraperCirclesMargin: 2.0,
    opening: 100.0,
    nSides: 32
}

let viewParameters = {
    show: null,
    opacity: 0.5,
    boardWidth: 420.0,
    boardMargin: 2.0,
}

let camera
let container = document.body
let scene, renderer
let materials = []
let meshesInOrder = []
let group = null
let bspDelta = 0.1 
let nPieces = 0

let paperGroup = null
let position = null
let lineHeight = 0


function initialize() {

    group = new THREE.Group()

    paperGroup = new paper.Group()
    position = new paper.Point(0, 0)
    paperGroup.strokeWidth = 1
    paperGroup.strokeColor = 'black'

    container = document.createElement( 'div' )
    document.body.insertBefore( container, document.body.firstChild )

    scene = new THREE.Scene()
    // scene.background = new THREE.Color( 0x000000 )
    camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 25, 1000 )
    camera.position.set( 400, 200, 0 );
    
    renderer = new THREE.WebGLRenderer( { antialias: true, preserveDrawingBuffer: true, alpha: true } )

    renderer.setPixelRatio( window.devicePixelRatio )
    renderer.setSize( window.innerWidth, window.innerHeight )
    container.appendChild( renderer.domElement )

    controls = new THREE.OrbitControls( camera, renderer.domElement )
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.screenSpacePanning = false;
    controls.minDistance = 100;
    controls.maxDistance = 500;
    controls.maxPolarAngle = Math.PI / 2;

    let radius = parameters.diameter / 2
    let height = parameters.interiorHeight + parameters.bearingHeight + parameters.thickness * 5
    let geometry = new THREE.CylinderBufferGeometry(radius, radius, height, Math.floor(parameters.nSides), 1)
    // material = new THREE.MeshPhongMaterial( { color: new THREE.Color( 0.25, 0.45, 0.89), flatShading: true } )
    // material = new THREE.MeshBasicMaterial( {color: new THREE.Color( 0.75, 0.45, 0.21)} );
    for(let i=0 ; i<6 ; i++) {
        let material = new THREE.MeshPhongMaterial( { color: new THREE.Color(Math.random(), Math.random(), Math.random()), emissive: 0x072534, side: THREE.DoubleSide, flatShading: true, transparent: true, opacity: 0.5 } )
        materials.push(material)
    }

    let mesh = new THREE.Mesh( geometry, materials[0] )

    group.add( mesh )
    scene.add( group )


    var lights = [];
    lights[ 0 ] = new THREE.PointLight( 0xffffff, 1, 0 );
    lights[ 1 ] = new THREE.PointLight( 0xffffff, 1, 0 );
    lights[ 2 ] = new THREE.PointLight( 0xffffff, 1, 0 );

    let distance = 100
    lights[ 0 ].position.set( 0, 2*distance, 0 );
    lights[ 1 ].position.set( distance, 2*distance, distance );
    lights[ 2 ].position.set( - distance, - 2*distance, - distance );

    scene.add( lights[ 0 ] );
    scene.add( lights[ 1 ] );
    scene.add( lights[ 2 ] );
    
    // var axesHelper = new THREE.AxesHelper( 50 );
    // scene.add( axesHelper );

    renderer.clear()

}

function randomMaterial() {
    return materials[Math.floor(Math.random()*materials.length)]
}

function generateCylinder(height, radius, open=false, position=null, material=randomMaterial()) {
    let nSides = Math.floor(parameters.nSides)
    let geometry = new THREE.CylinderGeometry(radius, radius, height, nSides, 1, open)
    let mesh = new THREE.Mesh( geometry, material)
    if(position != null) {
        mesh.position.copy(position)
    }
    mesh.position.y += height / 2
    return mesh;
}

function generateCylinderBSP(height, radius, position) {
    return THREE.CSG.fromMesh(generateCylinder(height, radius, position));
}

function generateTube(height, innerRadius, outerRadius, position=null, material=randomMaterial()) {
    let group = new THREE.Group()
    let innerMesh = generateCylinder(height, innerRadius, true, null, material)
    let outerMesh = generateCylinder(height, outerRadius, true, null, material)
    let nSides = Math.floor(parameters.nSides)
    let topGeometry = new THREE.RingBufferGeometry( innerRadius, outerRadius, nSides );
    let topMesh = new THREE.Mesh( topGeometry, material)
    topMesh.rotation.x += Math.PI / 2
    topMesh.position.y += height
    let bottomGeometry = new THREE.RingBufferGeometry( innerRadius, outerRadius, nSides );
    let bottomMesh = new THREE.Mesh( bottomGeometry, material )
    bottomMesh.rotation.x += Math.PI / 2
    group.add(innerMesh)
    group.add(outerMesh)
    group.add(topMesh)
    group.add(bottomMesh)
    if(position != null) {
        group.position.copy(position)
    }
    return group;
}


function generateTubeBSP(height, innerRadius, outerRadius, position=new THREE.Vector3(0, 0, 0)) {
    let innerCylinderPosition = position.clone()
    innerCylinderPosition.y -= bspDelta
    let innerCylinder = generateCylinderBSP(height + 2 * bspDelta, innerRadius, innerCylinderPosition);
    let outerCylinder = generateCylinderBSP(height, outerRadius, position);
    
    return outerCylinder.subtract(innerCylinder);
}

function generateTubes(thickness, innerRadius, outerRadius, n, bspOperation, material=null) {
    let tubes = new THREE.Group()
    for(let i=0 ; i<n ; i++) {
        // let tubeBSP = generateTubeBSP(thickness, innerRadius, outerRadius)
        // if(bspOperation != null) {
        //     tubeBSP = bspOperation(tubeBSP, i, n, thickness, innerRadius, outerRadius)
        // }
        let m = material != null ? material : randomMaterial()

        // let tube = THREE.CSG.toMesh(tubeBSP, material)
        let tube = generateTube(thickness, innerRadius, outerRadius, null, m)
        tube.position.y += i * thickness
        tubes.add(tube)
    }
    return tubes;
}

function bottomWallOperation(bsp, i, n, thickness, innerRadius, outerRadius) {
    if(i == n - 1) {
        let rotaryEncoderHolder = generateRotaryEncoderHolderBase(innerRadius, outerRadius, parameters.rotaryEncoderHolderWidth, parameters.thickness + 2 * bspDelta)
        rotaryEncoderHolder.position.y -= bspDelta
        // let rotaryEncoderHolderHole = new generateCylinderBSP(parameters.thickness + 2 * bspDelta, parameters.rotaryEncoderHolderHoleDiameter / 2, new THREE.Vector3(0, 0, -bspDelta))
        return bsp.subtract(THREE.CSG.fromMesh(rotaryEncoderHolder))
    }
    return bsp
}

function generateRotaryEncoderHolderBase(innerRadius, outerRadius, width, thickness, material=randomMaterial()) {
    let lx = innerRadius + (outerRadius - innerRadius) / 3
    let geometry = new THREE.BoxGeometry(2 * lx, thickness, width)
    let mesh = new THREE.Mesh(geometry, material)
    mesh.position.y += thickness / 2
    return mesh
}

function topWallOperation(bsp, i, n, thickness, innerRadius, outerRadius, material=randomMaterial()) {
    if(i > 1) {
        let width = innerRadius + 2 * bspDelta
        let geometry = new THREE.BoxGeometry(width, width, width)
        let mesh = new THREE.Mesh(geometry, material)
        mesh.position.y -= bspDelta
        mesh.position.x += - parameters.rotaryEncoderDOffset + width / 2
        return bsp.union(THREE.CSG.fromMesh(mesh))
    }
    return bsp
}

function generateRotaryEncoderHolder(innerRadius, outerRadius, width, thickness, nLayersBottom, rotaryEncoderHolderHoleDiameter, material=randomMaterial()) {
    let rotaryEncoderHolder = generateRotaryEncoderHolderBase(innerRadius, outerRadius, width, thickness, material)
    // let rotaryEncoderHolderHole = new generateCylinderBSP(thickness + 2 * bspDelta, rotaryEncoderHolderHoleDiameter / 2, new THREE.Vector3(0, 0, -bspDelta))
    // let mesh = rotaryEncoderHolder.subtract(THREE.CSG.fromMesh(rotaryEncoderHolderHole))
    // return THREE.CSG.toMesh(mesh, material)
    return rotaryEncoderHolder
}

function removeGroup(group) {
    for(let child of group.children.slice()) {
        removeGroup(child)
        group.remove(child)
        if(child.geometry) {
            child.geometry.dispose()
        }
    }
}

function generateMMIMM1() {
    removeGroup(group)

    let nLayersBottom = Math.floor(parameters.interiorHeight / parameters.thickness)
    let nLayersTop = Math.ceil(parameters.bearingHeight / parameters.thickness)

    parameters.interiorHeight = nLayersBottom * parameters.thickness

    let bottom = new THREE.Group()

    let totalHeight = parameters.interiorHeight + parameters.bearingHeight + parameters.thickness * 4
    let outerRadius = parameters.diameter / 2 - parameters.thickness
    let innerRadius = outerRadius - parameters.interiorWallThickness
    let bearingInnerRadius = parameters.bearingInnerDiameter / 2
    let bearingOuterRadius = parameters.bearingOuterDiameter / 2
    let bearingEdgeThickness = parameters.bearingEdgeThickness
    let wraperHeight = totalHeight / 2 - parameters.thickness - parameters.spaceBetweenTopAndBottom

    let bottomBottom = generateCylinder(parameters.thickness, parameters.diameter / 2)

    let bottomWall = generateTubes(parameters.thickness, innerRadius, outerRadius, nLayersBottom, bottomWallOperation)
    bottomWall.position.y += parameters.thickness

    let rotaryEncoderHolder = generateRotaryEncoderHolderBase(innerRadius, outerRadius, parameters.rotaryEncoderHolderWidth, parameters.thickness)
    rotaryEncoderHolder.position.y += (nLayersBottom - 2) * parameters.thickness

    let midWall = generateTube(parameters.thickness, bearingOuterRadius - bearingEdgeThickness, outerRadius)
    midWall.position.y += parameters.thickness + nLayersBottom * parameters.thickness
    let bottomTopOuterWallOuterRadius = bearingOuterRadius + (outerRadius-bearingOuterRadius) / 2 - parameters.spaceBetweenTopWalls
    let bottomTopOuterWall = generateTubes(parameters.thickness, bearingOuterRadius, bottomTopOuterWallOuterRadius, nLayersTop)
    bottomTopOuterWall.position.y += 2 * parameters.thickness + nLayersBottom * parameters.thickness
    let bottomTopCap = generateTube(parameters.thickness, bearingOuterRadius - bearingEdgeThickness, bottomTopOuterWallOuterRadius)
    bottomTopCap.position.y += 2 * parameters.thickness + nLayersBottom * parameters.thickness + nLayersTop * parameters.thickness
    let bottomWraper = generateTube(wraperHeight, outerRadius, parameters.diameter / 2)
    bottomWraper.position.y += parameters.thickness
    
    bottom.add(bottomBottom)
    bottom.add(bottomWall)
    bottom.add(rotaryEncoderHolder)
    bottom.add(midWall)
    bottom.add(bottomTopOuterWall)
    bottom.add(bottomTopCap)
    bottom.add(bottomWraper)

    let top = new THREE.Group()
    let topBottom = generateTube(parameters.thickness, parameters.rotaryEncoderDiameter / 2, bearingInnerRadius + bearingEdgeThickness)
    topBottom.position.y += parameters.thickness + nLayersBottom * parameters.thickness

    let topWall = generateTubes(parameters.thickness, parameters.rotaryEncoderDiameter / 2, bearingInnerRadius, nLayersTop, topWallOperation)
    topWall.position.y += 2 * parameters.thickness + nLayersBottom * parameters.thickness

    let topWallTop = generateCylinder(parameters.thickness, bearingOuterRadius)
    topWallTop.position.y += 2 * parameters.thickness + nLayersBottom * parameters.thickness + nLayersTop * parameters.thickness

    let topTop = generateCylinder(parameters.thickness, parameters.diameter / 2)
    let topZ = 3 * parameters.thickness + nLayersBottom * parameters.thickness + nLayersTop * parameters.thickness
    topTop.position.y += topZ
    
    let topOuterWall = generateTubes(parameters.thickness, bearingOuterRadius + (outerRadius-bearingOuterRadius) / 2 - parameters.spaceBetweenTopWalls, outerRadius, nLayersTop + 1)
    topOuterWall.position.y += 2 * parameters.thickness + nLayersBottom * parameters.thickness
    
    let topWraper = generateTube(wraperHeight, outerRadius, parameters.diameter / 2)
    topWraper.position.y += topZ - wraperHeight

    top.add(topBottom)
    top.add(topWall)
    top.add(topWallTop)
    top.add(topTop)
    top.add(topOuterWall)
    top.add(topWraper)

    top.position.y += parameters.opening

    let bearing = new THREE.Group()
    let bearingIn = generateTube(parameters.bearingHeight, bearingInnerRadius, bearingInnerRadius + bearingEdgeThickness)
    let bearingMid = generateTube(parameters.bearingHeight, bearingInnerRadius + bearingEdgeThickness, bearingOuterRadius - bearingEdgeThickness)
    let bearingOut = generateTube(parameters.bearingHeight, bearingOuterRadius - bearingEdgeThickness, bearingOuterRadius)

    bearing.add(bearingIn)
    bearing.add(bearingMid)
    bearing.add(bearingOut)

    bearing.position.y += 2 * parameters.thickness + nLayersBottom * parameters.thickness + parameters.opening / 2

    group.add( bottom )
    group.add( top )
    group.add( bearing )

    group.position.y = - (2 * parameters.thickness + nLayersBottom * parameters.thickness + parameters.opening / 2)

    nPieces = 0
    for(let child of group.children) {
        nPieces += child.children.length
    }
    
    if(viewParameters.show == null) {
        viewParameters.show = nPieces
    }

    hideParts()
}

function generatePaperCircle(position, radius) {
    let margin = viewParameters.boardMargin
    let circle = new paper.Path.Circle(position.add(margin + radius, margin + radius), radius)
    paperGroup.addChild(circle)

    position.x += 2 * radius + margin
    lineHeight = Math.max(lineHeight, 2 * radius)

    if(position.x > viewParameters.boardWidth) {
        position.y += lineHeight + margin
        position.x = 0
        lineHeight = 0
    }
}

function generatePaperTube(position, innerRadius, outerRadius) {
    let margin = viewParameters.boardMargin
    let center = position.add(margin + outerRadius, margin + outerRadius)
    let tube = new paper.CompoundPath({
        children: [
            new paper.Path.Circle({
                center: center,
                radius: outerRadius
            }),
            new paper.Path.Circle({
                center: center,
                radius: innerRadius
            })
        ],
        fillRule: 'evenodd'
    })

    paperGroup.addChild(tube)

    position.x += 2 * outerRadius + margin
    lineHeight = Math.max(lineHeight, 2 * outerRadius)

    if(position.x > viewParameters.boardWidth) {
        position.y += lineHeight + margin
        position.x = 0
        lineHeight = 0
    }
}

function generatePaperTubeNotch(position, innerRadius, outerRadius, notchSize) {
    let margin = viewParameters.boardMargin
    let center = position.add(margin + outerRadius, margin + outerRadius)
    let innerCircle = new paper.Path.Circle({
        center: center,
        radius: innerRadius
    })
    let notch = new paper.Path.Rectangle(center.subtract(2*innerRadius), center.add(-innerRadius+notchSize, 2*innerRadius))
    let notchCircle = innerCircle.subtract(notch)
    let tube = new paper.CompoundPath({
        children: [
            new paper.Path.Circle({
                center: center,
                radius: outerRadius
            }),
            notchCircle
        ],
        fillRule: 'evenodd'
    })

    paperGroup.addChild(tube)

    position.x += 2 * outerRadius + margin
    lineHeight = Math.max(lineHeight, 2 * outerRadius)

    if(position.x > viewParameters.boardWidth) {
        position.y += lineHeight + margin
        position.x = 0
        lineHeight = 0
    }
}


function generatePaperTubes(position, innerRadius, outerRadius, n) {
    for(let i=0 ; i<n ; i++) {
        generatePaperTube(position, innerRadius, outerRadius)
    }
}

function generatePaperTubesNotch(position, innerRadius, outerRadius, notchSize, n) {
    for(let i=0 ; i<n ; i++) {
        generatePaperTubeNotch(position, innerRadius, outerRadius, notchSize)
    }
}

function generatePaperRectangle(position, width, height) {
    let margin = viewParameters.boardMargin
    let rectangle = new paper.Path.Rectangle(position.add(margin, margin), position.add(margin + width, margin + height))
    paperGroup.addChild(rectangle)
    position.x += width + margin
    lineHeight = Math.max(lineHeight, height)
    if(position.x > viewParameters.boardWidth) {
        position.y += lineHeight + margin
        position.x = 0
        lineHeight = 0
    }
    return rectangle
}

function generatePaperRotaryEncoderHolderBase(position, innerRadius, outerRadius, width) {
    let lx = innerRadius + (outerRadius - innerRadius) / 3
    let rectnagle = generatePaperRectangle(position, 2 * lx, width)
    let circle = paper.Path.Circle(rectnagle.bounds.center, parameters.rotaryEncoderHolderHoleDiameter/2)
    paperGroup.addChild(circle)
}

function generatePaperWraper(position, radius, height) {
    let margin = viewParameters.boardMargin
    let p = position.add(margin, margin)
    let width = 2 * Math.PI * radius
    generatePaperRectangle(position, width, height)
    
    let nSteps = Math.floor(width / parameters.wraperStepSize)
    let wraperStepSize = width / nSteps
    
    for(let i=0 ; i<nSteps+1 ; i++) {
        
        let circle1 = new paper.Path.Circle(p.add(0, parameters.wraperStepHeight), parameters.wraperStepHeight / 2)
        paperGroup.addChild(circle1)

        if(i < nSteps) {

            let path1 = new paper.Path()
            path1.add(p.add(wraperStepSize/2, 0))
            path1.add(p.add(wraperStepSize/2, height - parameters.wraperStepHeight))
            paperGroup.addChild(path1)

            let path2 = new paper.Path()
            path2.add(p.add(wraperStepSize, height))
            path2.add(p.add(wraperStepSize, parameters.wraperStepHeight))
            paperGroup.addChild(path2)

            let circle2 = new paper.Path.Circle(path1.lastSegment.point, parameters.wraperStepHeight / 2)
            paperGroup.addChild(circle2)

        }

        let wraperNCircles = Math.floor(parameters.wraperNCircles)
        let yStep = (height - 2 * parameters.wraperStepHeight - 2 * parameters.wraperCirclesMargin) / wraperNCircles

        for(let j=0 ; j<wraperNCircles ; j++) {

            let y = parameters.wraperStepHeight + parameters.wraperCirclesMargin + (j + 0.5) * yStep
            let circle3 = new paper.Path.Circle(p.add(0, y), parameters.wraperCirclesSize / 2)
            paperGroup.addChild(circle3)

            if(i == nSteps) {
                continue
            }
            if(j == parameters.wraperNCircles - 1) {
                break
            }

            let circle4 = new paper.Path.Circle(p.add(wraperStepSize / 2, y + 0.5 * yStep), parameters.wraperCirclesSize / 2)
            paperGroup.addChild(circle4)

        }

        p.x += wraperStepSize
    }
    
}

function generateMMIMM() {
    removeGroup(group)
    paperGroup.removeChildren()

    position.x = 0
    position.y = 0
    lineHeight = 0

    let nLayersBottom = Math.floor(parameters.interiorHeight / parameters.thickness)
    let nLayersTop = Math.floor(parameters.rotaryEncoderHeight / parameters.thickness)

    parameters.interiorHeight = nLayersBottom * parameters.thickness

    let bottom = new THREE.Group()

    let totalHeight = parameters.interiorHeight + parameters.thickness * 4
    let outerRadius = parameters.diameter / 2 - parameters.thickness
    let innerRadius = outerRadius - parameters.interiorWallThickness
    let wraperHeight = totalHeight - 2 * parameters.thickness - parameters.spaceBetweenTopAndBottom
    let bearingInnerRadius = parameters.bearingInnerDiameter / 2
    let bearingOuterRadius = parameters.bearingOuterDiameter / 2
    let bearingEdgeThickness = parameters.bearingEdgeThickness

    let bottomBottom = generateCylinder(parameters.thickness, parameters.diameter / 2)
    
    generatePaperCircle(position, parameters.diameter / 2)
    

    let bottomWall = generateTubes(parameters.thickness, innerRadius, outerRadius, nLayersBottom)
    bottomWall.position.y += parameters.thickness

    generatePaperTubes(position, innerRadius, outerRadius, nLayersBottom)

    let rotaryEncoderHolder = generateRotaryEncoderHolderBase(innerRadius, outerRadius, parameters.rotaryEncoderHolderWidth, parameters.thickness)
    rotaryEncoderHolder.position.y += (nLayersBottom - 2) * parameters.thickness


    let bottomTopCap = generateTube(parameters.thickness, innerRadius - bearingEdgeThickness, outerRadius)
    bottomTopCap.position.y += (nLayersBottom + 1) * parameters.thickness
    
    generatePaperTube(position, innerRadius - bearingEdgeThickness, outerRadius)

    let bottomWraper = generateTube(wraperHeight, outerRadius, parameters.diameter / 2)
    bottomWraper.position.y += parameters.thickness
    
    bottom.add(bottomBottom)
    bottom.add(bottomWall)
    bottom.add(rotaryEncoderHolder)
    bottom.add(bottomTopCap)
    bottom.add(bottomWraper)

    let top = new THREE.Group()
    
    let topWall = generateTubes(parameters.thickness, parameters.rotaryEncoderDiameter / 2, bearingInnerRadius, nLayersTop)
    topWall.position.y += nLayersBottom * parameters.thickness

    generatePaperTubesNotch(position, parameters.rotaryEncoderDiameter / 2, bearingInnerRadius, parameters.rotaryEncoderNotchSize, nLayersTop)

    let topBottom = generateTube(parameters.thickness, innerRadius - parameters.spaceBetweenTopWalls - 2 * parameters.bearingEdgeThickness, innerRadius - parameters.spaceBetweenTopWalls)
    topBottom.position.y += nLayersBottom * parameters.thickness

    generatePaperTube(position, innerRadius - parameters.spaceBetweenTopWalls - 2 * parameters.bearingEdgeThickness, innerRadius - parameters.spaceBetweenTopWalls)

    let topMid = generateTube(parameters.thickness, innerRadius - parameters.spaceBetweenTopWalls - 2 * parameters.bearingEdgeThickness, innerRadius - parameters.bearingEdgeThickness - parameters.spaceBetweenTopWalls)
    topMid.position.y += (nLayersBottom + 1) * parameters.thickness

    generatePaperTube(position, innerRadius - parameters.spaceBetweenTopWalls - 2 * parameters.bearingEdgeThickness, innerRadius - parameters.bearingEdgeThickness - parameters.spaceBetweenTopWalls)

    let topTop = generateTube(parameters.thickness, innerRadius - parameters.spaceBetweenTopWalls - 2 * parameters.bearingEdgeThickness, innerRadius - parameters.spaceBetweenTopWalls)
    topTop.position.y += (nLayersBottom + 2) * parameters.thickness

    generatePaperTube(position, innerRadius - parameters.spaceBetweenTopWalls - 2 * parameters.bearingEdgeThickness, innerRadius - parameters.spaceBetweenTopWalls)

    let topCap = generateCylinder(parameters.thickness, parameters.diameter / 2)
    let topZ = (nLayersBottom + 3) * parameters.thickness
    topCap.position.y += topZ

    generatePaperCircle(position, parameters.diameter / 2)

    top.add(topWall)
    top.add(topBottom)
    top.add(topMid)
    top.add(topTop)
    top.add(topCap)

    top.position.y += parameters.opening

    group.add( bottom )
    group.add( top )

    group.position.y = - (2 * parameters.thickness + nLayersBottom * parameters.thickness + parameters.opening / 2)

    nPieces = 0
    for(let child of group.children) {
        nPieces += child.children.length
    }
    
    if(viewParameters.show == null) {
        viewParameters.show = nPieces
    }

    hideParts()

    generatePaperRotaryEncoderHolderBase(position, innerRadius, outerRadius, parameters.rotaryEncoderHolderWidth)
    generatePaperWraper(position, parameters.diameter / 2, wraperHeight)

    paperGroup.position = paper.view.bounds.center //.subtract(paperGroup.bounds.size.divide(2))

    let frameMargin = 5
    let frameRectangle = new paper.Rectangle(paperGroup.bounds.left-frameMargin, paperGroup.bounds.top-frameMargin, Math.ceil(paperGroup.bounds.width+2*frameMargin), Math.ceil(paperGroup.bounds.height+2*frameMargin))
    let frame = new paper.Path.Rectangle(frameRectangle)
    paperGroup.addChild(frame)
    frame.sendToBack()

    for(let child of paperGroup.children) {
        child.strokeWidth = 1
        child.strokeColor = 'black'
    }

    console.log(paperGroup.bounds.width, paperGroup.bounds.height)

    

    let margin = 20
    let groupRatio = paperGroup.bounds.width / paperGroup.bounds.height
    let viewRatio = paper.view.bounds.width / paper.view.bounds.height
    if(groupRatio > viewRatio) {
        paperGroup.scale( ( paper.view.bounds.width - margin ) / paperGroup.bounds.width)
    } else {
        paperGroup.scale( ( paper.view.bounds.height - margin ) / paperGroup.bounds.height)
    }
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize( window.innerWidth, window.innerHeight )
}

function animate() {
    requestAnimationFrame( animate )
    controls.update()
    render()
}


function render() {
    renderer.render( scene, camera )
}

let hideParts = ()=> {

    let i = 0
    for(let child of group.children) {
        for(let grandchild of child.children) {
            grandchild.visible = i <= viewParameters.show
            i++
        }
    }
}

let main = ()=> {

    var canvas = document.getElementById('canvas')
    paper.setup(canvas)

    initialize()
    generateMMIMM()

    window.addEventListener("resize", onWindowResize, false )


    var gui = new dat.GUI({hideable: true})

    for(let name in parameters) {
        gui.add(parameters, name, 0, 100, 0.01).onChange(()=> generateMMIMM())
    }

    gui.add(viewParameters, 'show', 0, nPieces, 1).onChange(hideParts)

    gui.add(viewParameters, 'opacity', 0, 1, 0.01).onChange((value)=> {
        for(let m of materials) {
            m.opacity = value
        }
    })

    gui.add(viewParameters, 'boardWidth', 0, 1000, 1).onChange((value)=> {
        generateMMIMM()
    })

    gui.add(viewParameters, 'boardMargin', 0, 10, 1).onChange((value)=> {
        generateMMIMM()
    })

    animate()
}

document.addEventListener("DOMContentLoaded", main)




